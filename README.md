# Modular system back

Бэкенд часть приложения для контроля успеваемости студентов и управления базой знаний для лабораторных работ.

## Installation

```bash
$ npm install
```

## Initializing db

```bash
#initialization
$ npm run prisma:migrate

#seeds
$ npx prisma db seed

```

## Initializing db in Docker

```bash
#create container
$ docker-compose up -d

#create prisma client
$ npx prisma generate

#apply the existing migration
$ npx prisma migrate dev

#seeds
$ npx prisma db seed

#track the database
$ npx prisma studio

#create new migration (after change the schema.prisma file)
$ npm run prisma:migrate:create -- <migration_name>
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Create an https certificate

1. Для начала необходимо установить openssl: https://slproweb.com/products/Win32OpenSSL.html.

2. После установки лучше перезапустить ПК. Если после перезагрузки в консоли команда openssl не определяется, то необходимо в переменную среды **Path** добавить путь к папке **bin**, установленного **Openssl**.

3. Далее в корне проекта создаём папку **https**

4. Переходим в созданную папку. В консоли генерируем приватный ключ: `openssl genrsa -out cert.key 2048`

5. Далее генерируем сертификат на основе полученного ключа: `openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout cert.key -out cert.crt -subj "/CN=localhost" -addext "subjectAltName=DNS:localhost,DNS:localhost,IP:127.0.0.1"`

6. Теперь, чтобы браузер доверял сертификату, необходимо открыть **настройки браузера** -> **конфиденциальность и безопасность** -> **безопасность**: chrome://settings/security

7. Пункт Настроить сертификаты (Управление настройками и сертификатами HTTPS/SSL).

8. В открывшемся меню необходимо вместо "Личные" выбрать "Доверенные центры сертификации" и нажать кнопку импорт.

9. В открывшемся окне, импортируем сертификат (cert.crt).

10. Перезагружаем браузер, проверяем, можем ли мы открыть сайт без предупреждений.
