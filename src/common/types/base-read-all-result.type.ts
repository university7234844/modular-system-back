import { ApiProperty } from '@nestjs/swagger';

export class ReadAllResult<T> {
  @ApiProperty({ description: 'Number of all records' })
  totalRecordsNumber: number;

  @ApiProperty({ description: 'Current records', type: [Object] })
  records: T[];
}
