import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class Payload {
  @IsInt()
  @IsNotEmpty()
  sub: number;

  @IsString()
  @IsNotEmpty()
  roles: string[];
}
