/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'isDateFormat', async: false })
export class IsDateFormat implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(text);
  }

  defaultMessage(args: ValidationArguments) {
    return 'The date format is incorrect. The YYYY-MM-DD format is expected';
  }
}
