import { ApiProperty } from '@nestjs/swagger';

import { defaultSorting } from '../constants';

export class SortingDto {
  @ApiProperty({
    name: 'sorting[column]',
    required: false,
    description: 'Sorting column',
    type: 'string',
    default: 'id',
    example: 'id',
  })
  column: string = defaultSorting.column;

  @ApiProperty({
    name: 'sorting[direction]',
    required: false,
    description: 'Sorting direction',
    type: 'string',
    enum: ['desc', 'asc'],
    default: 'asc',
    example: 'asc',
  })
  direction: 'desc' | 'asc' = defaultSorting.direction;
}
