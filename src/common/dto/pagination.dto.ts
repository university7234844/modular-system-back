import { ApiProperty } from '@nestjs/swagger';

import { defaultPagination } from '../constants';

export class PaginationDto {
  @ApiProperty({
    required: false,
    description: 'Pagination page number',
    type: 'number',
    default: 1,
    example: 1,
  })
  page: number = defaultPagination.page;

  @ApiProperty({
    required: false,
    description: 'Pagination number of entity',
    type: 'number',
    default: 20,
    example: 20,
  })
  size: number = defaultPagination.size;

  get offset(): number {
    return (this.page - 1) * this.size;
  }
}
