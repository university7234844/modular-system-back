import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { PaginationDto } from './pagination.dto';
import { SortingDto } from './sorting.dto';

export class BaseReadAllDto {
  @IsOptional()
  @ApiProperty({ required: false, type: PaginationDto })
  pagination?: PaginationDto;

  @IsOptional()
  @ApiProperty({ required: false, type: SortingDto })
  sorting?: SortingDto;
}
