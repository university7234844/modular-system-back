export enum ControlFormEnum {
  EXAM = 'Exam',
  CREDIT_TEST = 'Credit test',
  DIFFERENTIATED_CREDIT_TEST = 'Differentiated credit test',
}
