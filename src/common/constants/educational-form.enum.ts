export enum EducationalFormEnum {
  FULL_TIME = 'Full time',
  PART_TIME = 'Part time',
}
