import { TeacherPositions } from '@prisma/client';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class TeacherPositionEntity implements TeacherPositions {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;
}
