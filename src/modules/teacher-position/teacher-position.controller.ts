import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiParam,
} from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';

import { TeacherPositionService } from './teacher-position.service';
import { TeacherPositionResponse } from './types';
import {
  CreateTeacherPositionDto,
  ReadAllTeacherPositionDto,
  UpdateTeacherPositionDto,
} from './dto';

@Controller('teacher-position')
export class TeacherPositionController {
  constructor(private readonly service: TeacherPositionService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'Success',
    type: TeacherPositionResponse,
  })
  async create(
    @Body() createDto: CreateTeacherPositionDto,
  ): Promise<TeacherPositionResponse> {
    const position = await this.service.create(createDto);

    return new TeacherPositionResponse(position);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  title: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              title: 'Преподаватель',
            },
            {
              id: 2,
              title: 'Старший преподаватель',
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllTeacherPositionDto,
  ): Promise<ReadAllResult<TeacherPositionResponse>> {
    const positions = await this.service.readAll(readAllOptions);

    return {
      totalRecordsNumber: positions.totalRecordsNumber,
      records: positions.records.map(
        (record) => new TeacherPositionResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get teacher position by id',
    type: TeacherPositionResponse,
  })
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<TeacherPositionResponse> {
    const position = await this.service.readById(id);

    return new TeacherPositionResponse(position);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Update teacher position by id',
    type: TeacherPositionResponse,
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateTeacherPositionDto,
  ): Promise<TeacherPositionResponse> {
    const position = await this.service.update(id, updateDto);

    return new TeacherPositionResponse(position);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Delete teacher position by id',
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.service.delete(id);
  }
}
