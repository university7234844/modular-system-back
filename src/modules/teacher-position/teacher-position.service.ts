import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { DatabaseService } from 'src/database/database.service';
import { ReadAllResult } from 'src/common/types';

import { TeacherPositionEntity } from './entities';
import {
  CreateTeacherPositionDto,
  ReadAllTeacherPositionDto,
  UpdateTeacherPositionDto,
} from './dto';

@Injectable()
export class TeacherPositionService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createDto: CreateTeacherPositionDto,
  ): Promise<TeacherPositionEntity> {
    const existedPosition = await this.prisma.teacherPositions.findUnique({
      where: { ...createDto },
    });

    if (existedPosition) {
      throw new BadRequestException('The specified position already exists');
    }

    const position = await this.prisma.teacherPositions.create({
      data: createDto,
    });

    return position;
  }

  async readAll(
    readAllOptions: ReadAllTeacherPositionDto,
  ): Promise<ReadAllResult<TeacherPositionEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const positions = await this.prisma.teacherPositions.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.teacherPositions.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: positions,
    };
  }

  async readById(id: number): Promise<TeacherPositionEntity> {
    const position = await this.prisma.teacherPositions.findUnique({
      where: { id },
    });

    return position;
  }

  async update(
    id: number,
    updateDto: UpdateTeacherPositionDto,
  ): Promise<TeacherPositionEntity> {
    const doesPositionExist = await this.readById(id);

    if (!doesPositionExist) {
      throw new BadRequestException('The specified position does not exist');
    }

    const existedPositions = await this.prisma.teacherPositions.findMany({
      where: { ...updateDto },
    });

    if (existedPositions.length) {
      throw new BadRequestException('The specified position already exists');
    }

    const position = await this.prisma.teacherPositions.update({
      where: { id },
      data: { ...updateDto },
    });

    return position;
  }

  async delete(id: number): Promise<void> {
    const doesPositionExist = await this.readById(id);

    if (!doesPositionExist) {
      throw new BadRequestException('The specified position does not exist');
    }

    await this.prisma.teacherPositions.delete({ where: { id } });
  }
}
