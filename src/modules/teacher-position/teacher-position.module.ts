import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';
import { TeacherPositionService } from './teacher-position.service';
import { TeacherPositionController } from './teacher-position.controller';

@Module({
  imports: [DatabaseModule],
  providers: [
    TeacherPositionService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [TeacherPositionController],
  exports: [TeacherPositionService],
})
export class TeacherPositionModule {}
