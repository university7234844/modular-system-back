import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllTeacherPositionDto extends BaseReadAllDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Position title',
    example: 'Преподаватель',
  })
  title?: string;
}
