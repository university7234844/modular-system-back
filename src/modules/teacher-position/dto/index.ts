export * from './create-teacher-position.dto';
export * from './read-all-teacher-position.dto';
export * from './update-teacher-position.dto';
