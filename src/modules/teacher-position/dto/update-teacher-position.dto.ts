import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateTeacherPositionDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Position title',
    example: 'Преподаватель',
  })
  title?: string;
}
