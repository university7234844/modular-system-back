import { ApiProperty } from '@nestjs/swagger';

import { TeacherPositionEntity } from '../entities';

export class TeacherPositionResponse {
  @ApiProperty({ description: "Specialty's id", example: 1 })
  id: number;

  @ApiProperty({
    description: "Positions's title",
    example: 'Преподаватель',
  })
  title: string;

  constructor(position: TeacherPositionEntity) {
    this.id = position.id;
    this.title = position.title;
  }
}
