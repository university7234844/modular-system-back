import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateTeacherDto {
  @IsNumber()
  @IsNotEmpty()
  userId: number;
}
