import { IsNumber, IsOptional } from 'class-validator';

export class UpdateTeacherDto {
  @IsNumber()
  @IsOptional()
  teacherPositionId?: number;

  @IsNumber()
  @IsOptional()
  academicDegreeId?: number;

  @IsNumber()
  @IsOptional()
  academicRankId?: number;
}
