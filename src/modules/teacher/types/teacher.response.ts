import { TeacherEntity } from '../entities';

export class TeacherResponse {
  id: number;
  userId: number;
  teacherPositionId: number | null;
  academicDegreeId: number | null;
  academicRankId: number | null;

  constructor(teacher: TeacherEntity) {
    this.id = teacher.id;
    this.userId = teacher.userId;
    this.teacherPositionId = teacher.teacherPositionId;
    this.academicDegreeId = teacher.academicDegreeId;
    this.academicRankId = teacher.academicRankId;
  }
}
