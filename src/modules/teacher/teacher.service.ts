import { BadRequestException, Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';
import { CreateTeacherDto, UpdateTeacherDto } from './dto';
import { TeacherEntity } from './entities';

@Injectable()
export class TeacherService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(createTeacherDto: CreateTeacherDto): Promise<TeacherEntity> {
    const { userId } = createTeacherDto;

    const existedTeacher = await this.prisma.teachers.findFirst({
      where: { userId },
    });

    if (existedTeacher) {
      throw new BadRequestException(
        `Teacher for User with UserId=${userId} already exist`,
      );
    }

    const teacher = await this.prisma.teachers.create({ data: { userId } });

    return teacher;
  }

  async update(
    id: number,
    updateDto: UpdateTeacherDto,
  ): Promise<TeacherEntity> {
    const teacher = await this.prisma.teachers.update({
      where: { id },
      data: { ...updateDto },
    });

    return teacher;
  }
}
