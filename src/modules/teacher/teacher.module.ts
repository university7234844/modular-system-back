import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';

import { TeacherService } from './teacher.service';
import { TeacherController } from './teacher.controller';

@Module({
  imports: [DatabaseModule],
  providers: [
    TeacherService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [TeacherController],
  exports: [TeacherService],
})
export class TeacherModule {}
