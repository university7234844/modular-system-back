import { Teachers } from '@prisma/client';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class TeacherEntity implements Teachers {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsNumber()
  @IsOptional()
  teacherPositionId: number | null;

  @IsNumber()
  @IsOptional()
  academicDegreeId: number | null;

  @IsNumber()
  @IsOptional()
  academicRankId: number | null;

  @IsNumber()
  @IsNotEmpty()
  userId: number;
}
