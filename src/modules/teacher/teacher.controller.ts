import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Put,
} from '@nestjs/common';
import { TeacherService } from './teacher.service';
import { UpdateTeacherDto } from './dto';
import { TeacherResponse } from './types';

@Controller('teacher')
export class TeacherController {
  constructor(private readonly service: TeacherService) {}

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateTeacherDto,
  ): Promise<TeacherResponse> {
    const teacher = await this.service.update(id, updateDto);
    return teacher;
  }
}
