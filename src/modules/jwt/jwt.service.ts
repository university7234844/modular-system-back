import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService as NestJwtService } from '@nestjs/jwt';
import { Cron, CronExpression } from '@nestjs/schedule';

import { DatabaseService } from 'src/database/database.service';
import { Payload } from 'src/common/types';

import { TokenEntity } from './entities';
import { parseTokenExpiration } from 'src/common/utils';

@Injectable()
export class JwtService {
  private accessSecret: string;
  private accessDuration: string;
  private refreshSecret: string;
  private refreshDuration: string;

  constructor(
    private readonly nestJwtService: NestJwtService,
    private readonly config: ConfigService,
    private readonly prisma: DatabaseService,
  ) {
    this.accessSecret = this.config.get('JWT_ACCESS_SECRET');
    this.accessDuration = this.config.get('JWT_ACCESS_DURATION');

    this.refreshSecret = this.config.get('JWT_REFRESH_SECRET');
    this.refreshDuration = this.config.get('JWT_REFRESH_DURATION');
  }

  async generateAccessJwt(payload: Payload): Promise<string> {
    const accessToken = await this.nestJwtService.signAsync(payload, {
      secret: this.accessSecret,
      expiresIn: this.accessDuration,
    });
    return accessToken;
  }

  async generateRefreshJwt(payload: Payload): Promise<string> {
    const refreshToken = await this.nestJwtService.signAsync(payload, {
      secret: this.refreshSecret,
      expiresIn: this.refreshDuration,
    });
    return refreshToken;
  }

  // refresh token from database
  async readUserToken(
    userId: number,
    refreshToken: string,
  ): Promise<TokenEntity> {
    const token = await this.prisma.tokens.findFirst({
      where: {
        refreshToken: refreshToken,
        userId: userId,
      },
    });
    return token;
  }

  // save refresh token to database
  async createUserToken(
    userId: number,
    refreshToken: string,
  ): Promise<TokenEntity> {
    const refreshTokenDuration = this.refreshDuration;
    const tokenExpiration = parseTokenExpiration(refreshTokenDuration);
    const expirationDate = new Date(new Date().getTime() + tokenExpiration);

    const token = await this.prisma.tokens.create({
      data: {
        refreshToken: refreshToken,
        userId: userId,
        expirationDate: expirationDate,
      },
    });
    return token;
  }

  // delete refresh token from database
  async deleteUserToken(userId: number, refreshToken: string): Promise<void> {
    await this.prisma.tokens.deleteMany({
      where: {
        refreshToken: refreshToken,
        userId: userId,
      },
    });
  }

  async deleteAllUserTokens(userId: number): Promise<void> {
    await this.prisma.tokens.deleteMany({
      where: {
        userId: userId,
      },
    });
  }

  // clear the database of outdated tokens
  @Cron(CronExpression.EVERY_WEEK)
  async cleanupExpiredTokens(): Promise<void> {
    await this.prisma.tokens.deleteMany({
      where: {
        expirationDate: {
          lte: new Date(),
        },
      },
    });
  }
}
