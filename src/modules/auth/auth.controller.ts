import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';

import { setCookie } from 'src/common/utils';
import { CookieEnum } from 'src/common/constants';
import { GetPayload } from 'src/common/decorators';
import { Payload } from 'src/common/types';

import { LoginDto, RegisterDto } from './dto';
import { AuthService } from './auth.service';
import { RefreshJwtGuard } from './guards';
import { GetTokens } from './decorators';
import { AuthResponse } from './types';
import { UserResponse } from '../user/types';

@Controller('auth')
@ApiTags('Authorization')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({ description: 'Success', type: UserResponse })
  async register(@Body() registerDto: RegisterDto): Promise<UserResponse> {
    const user = await this.authService.register(registerDto);
    return new UserResponse(user);
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  @ApiCreatedResponse({ description: 'Success', type: AuthResponse })
  async login(
    @Body() loginDto: LoginDto,
    @Res({ passthrough: true }) res: Response,
  ): Promise<AuthResponse> {
    const authServiceResponse = await this.authService.login(loginDto);
    setCookie(res, CookieEnum.AUTH, authServiceResponse.jwts);
    return new AuthResponse(authServiceResponse);
  }

  @UseGuards(RefreshJwtGuard)
  @Post('logout')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({ description: 'Success' })
  async logout(
    @GetPayload() payload: Payload,
    @GetTokens('refreshToken') refreshToken: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<void> {
    await this.authService.logout(payload, refreshToken);
    setCookie(res, CookieEnum.AUTH, null);
  }

  @UseGuards(RefreshJwtGuard)
  @Post('refresh')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Success', type: AuthResponse })
  async refreshJwt(
    @GetPayload() payload: Payload,
    @GetTokens('refreshToken') refreshToken: string,
    @Res({ passthrough: true }) res: Response,
  ): Promise<AuthResponse> {
    const authServiceResponse = await this.authService.refreshJwt(
      payload,
      refreshToken,
    );
    setCookie(res, CookieEnum.AUTH, authServiceResponse.jwts);
    return new AuthResponse(authServiceResponse);
  }

  @UseGuards(RefreshJwtGuard)
  @Post('logout-from-all-devices')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({ description: 'Success' })
  async logoutFromAllDevices(
    @GetPayload() payload: Payload,
    @Res({ passthrough: true }) res: Response,
  ): Promise<void> {
    await this.authService.logoutFromAllDevices(payload);
    setCookie(res, CookieEnum.AUTH, null);
  }
}
