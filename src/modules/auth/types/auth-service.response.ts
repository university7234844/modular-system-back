import { IsNotEmptyObject, IsObject } from 'class-validator';

import { UserEntity } from 'src/modules/user/entities';

import { JwtResponse } from './jwt.response';
// It's inner dto witch use only for auth
// You shouldn't describe it for swagger

export class AuthServiceResponse {
  @IsObject()
  @IsNotEmptyObject({ nullable: false })
  jwts: JwtResponse;

  @IsObject()
  @IsNotEmptyObject({ nullable: false })
  user: UserEntity;
}
