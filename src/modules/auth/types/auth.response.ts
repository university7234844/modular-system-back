import { UserResponse } from 'src/modules/user/types';
import { AuthServiceResponse } from './auth-service.response';
import { ApiProperty } from '@nestjs/swagger';

// It's inner dto witch use only for auth
// You shouldn't describe it for swagger

export class AuthResponse {
  @ApiProperty({
    description: 'JWT Access',
    type: 'string',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInJvbGVzIjpbIkFkbWluIiwiVGVhY2hlciJdLCJpYXQiOjE3MTEyMTgzNTMsImV4cCI6MTcxMTIxODQxM30.bGt3H3Qts6XdD6XuvjxLIQAlICFwwuUQqfFfUW-tPmY',
  })
  accessToken: string;

  @ApiProperty({
    description: 'JWT Access',
    type: UserResponse,
  })
  user: UserResponse;

  constructor(authServiceResponse: AuthServiceResponse) {
    this.accessToken = authServiceResponse.jwts.accessToken;
    this.user = new UserResponse(authServiceResponse.user);
  }
}
