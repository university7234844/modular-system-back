import { IsNotEmpty, IsString } from 'class-validator';
// It's inner dto witch use only for auth
// You shouldn't describe it for swagger

export class JwtResponse {
  @IsString()
  @IsNotEmpty()
  accessToken: string;

  @IsString()
  @IsNotEmpty()
  refreshToken: string;
}
