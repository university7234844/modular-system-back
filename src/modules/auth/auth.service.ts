import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { compare, genSalt, hash } from 'bcrypt';

import { Payload } from 'src/common/types';

import { UserEntity } from '../user/entities';

import { UserService } from '../user/user.service';
import { RoleService } from '../role/role.service';
import { JwtService } from '../jwt/jwt.service';

import { LoginDto, RegisterDto } from './dto';
import { AuthServiceResponse, JwtResponse } from './types';
import { RoleEnum } from 'src/common/constants';
import { TeacherService } from '../teacher/teacher.service';
import { StudentService } from '../student/student.service';

@Injectable()
export class AuthService {
  private readonly passwordSalt: string;

  constructor(
    private readonly config: ConfigService,
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
    private readonly roleService: RoleService,
    private readonly teacherService: TeacherService,
    private readonly studentService: StudentService,
  ) {
    this.passwordSalt = this.config.get<string>('PASSWORD_SALT');
  }

  async register(registerDto: RegisterDto): Promise<UserEntity> {
    const { login, fullName, password, sex, birthday, roleNames } = registerDto;

    const existedUser = await this.userService.readByLogin(login);
    if (existedUser) {
      throw new BadRequestException('User with this login already exists');
    }

    const salt = await genSalt(10);
    const passwordHash = await hash(password + this.passwordSalt, salt);

    const roles = await this.roleService.readAllByName(roleNames);

    const isNotValidRoles =
      (roleNames.includes(RoleEnum.TEACHER) &&
        roleNames.includes(RoleEnum.STUDENT)) ||
      (roleNames.includes(RoleEnum.STUDENT) &&
        roleNames.includes(RoleEnum.ADMIN));

    //пользователь не может быть сразу преподавателем и студентом или студентом и админом
    if (roles.length == 0 || isNotValidRoles) {
      throw new BadRequestException(`Can't create user with such user data`);
    }

    const user = await this.userService.create({
      login,
      fullName,
      passwordHash,
      roles,
      sex,
      birthday,
    });

    if (roleNames.includes(RoleEnum.TEACHER)) {
      await this.teacherService.create({ userId: user.id });
    }

    if (roleNames.includes(RoleEnum.STUDENT)) {
      await this.studentService.create({ userId: user.id });
    }

    return user;
  }

  async login(loginDto: LoginDto): Promise<AuthServiceResponse> {
    const { login, password } = loginDto;
    const user = await this.validateLogin(login, password);

    const jwts = await this.generateJwts(
      user.id,
      user.roles.map((obj) => obj.role.name),
    );
    await this.jwtService.createUserToken(user.id, jwts.refreshToken);
    return { jwts, user };
  }

  async logout(payload: Payload, refreshToken: string): Promise<void> {
    await this.jwtService.deleteUserToken(payload.sub, refreshToken);
  }

  async refreshJwt(
    payload: Payload,
    refreshToken: string,
  ): Promise<AuthServiceResponse> {
    const token = await this.jwtService.readUserToken(
      payload.sub,
      refreshToken,
    );
    if (!token) {
      await this.jwtService.deleteAllUserTokens(payload.sub);
      throw new UnauthorizedException(
        `There is no token, for security reasons you need to log in to the service again on all devices using your email and password`,
      );
    }

    if (token.expirationDate < new Date()) {
      await this.jwtService.deleteUserToken(payload.sub, refreshToken);
      throw new UnauthorizedException(
        `The token's lifetime has expired, log in to your account using your login and password`,
      );
    }

    await this.jwtService.deleteUserToken(payload.sub, refreshToken);
    const jwts = await this.generateJwts(payload.sub, payload.roles);
    await this.jwtService.createUserToken(payload.sub, jwts.refreshToken);

    const user = await this.userService.readById(payload.sub);

    return { jwts, user };
  }

  async logoutFromAllDevices(payload: Payload): Promise<void> {
    await this.jwtService.deleteAllUserTokens(payload.sub);
  }

  private async validateLogin(
    login: string,
    password: string,
  ): Promise<UserEntity> {
    const existedUser = await this.userService.readByLogin(login);
    if (
      existedUser &&
      (await compare(password + this.passwordSalt, existedUser.passwordHash))
    ) {
      return existedUser;
    }

    throw new UnauthorizedException('Incorrect login and/or password entered');
  }

  private async generateJwts(
    userId: number,
    userRoles: string[],
  ): Promise<JwtResponse> {
    const payload = { sub: userId, roles: userRoles };
    const accessToken = await this.jwtService.generateAccessJwt(payload);
    const refreshToken = await this.jwtService.generateRefreshJwt(payload);
    return { accessToken, refreshToken };
  }
}
