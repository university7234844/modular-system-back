import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { UserModule } from '../user/user.module';
import { RoleModule } from '../role/role.module';
import { JwtModule } from '../jwt/jwt.module';
import { TeacherModule } from '../teacher/teacher.module';
import { StudentModule } from '../student/student.module';

import { RefreshJwtStrategy } from './strategies';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule,
    UserModule,
    RoleModule,
    JwtModule,
    TeacherModule,
    StudentModule,
  ],
  providers: [
    AuthService,
    RefreshJwtStrategy,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [AuthController],
})
export class AuthModule {}
