import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: "User's login",
    example: 'SergeyErmochenko',
  })
  login: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: "User's password",
    example: 'qqqqq',
  })
  password: string;
}
