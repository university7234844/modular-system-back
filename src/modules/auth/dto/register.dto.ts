import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsArray,
  IsIn,
  IsNotEmpty,
  IsString,
  Validate,
} from 'class-validator';
import { RoleEnum, SexEnum } from 'src/common/constants';
import { IsDateFormat } from 'src/common/validators';

export class RegisterDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: "User's login, should be unique",
    example: 'SergeyErmochenko',
  })
  login: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: "User's password, should be strong!",
    example: 'qqqqq',
  })
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description:
      "User's full name, should contain the last name first name and patronymic (patronymic is optional)",
    example: 'Сергей Ермоченко',
  })
  fullName: string;

  @IsIn([SexEnum.MALE, SexEnum.FEMALE])
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: "User's sex, valid only Male and Female strings",
    example: 'Male',
  })
  sex: string;

  @IsString()
  @IsNotEmpty()
  @Validate(IsDateFormat)
  @ApiProperty({
    required: true,
    description: "User's birthday date, valid format is YYYY-MM-DD",
    example: '1980-10-08',
  })
  birthday: string;

  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @ApiProperty({
    required: true,
    type: [String],
    description:
      "User's roles (only role name), should describe permissions in API!",
    example: [RoleEnum.ADMIN, RoleEnum.TEACHER],
  })
  roleNames: string[];
}
