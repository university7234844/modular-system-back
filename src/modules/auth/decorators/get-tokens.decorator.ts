import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { CookieEnum } from 'src/common/constants';

export const GetTokens = createParamDecorator(
  (data: any, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    const tokens = request?.cookies[CookieEnum.AUTH];

    if (!tokens) {
      return null;
    }

    if (data) {
      return tokens[data];
    }

    return tokens;
  },
);
