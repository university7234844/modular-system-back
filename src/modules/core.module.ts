import { Module } from '@nestjs/common';

import { DisciplineModule } from './discipline/discipline.module';
import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';
import { JwtModule } from './jwt/jwt.module';
import { AuthModule } from './auth/auth.module';
import { SpecialtyModule } from './specialty/specialty.module';
import { TeacherModule } from './teacher/teacher.module';
import { StudentModule } from './student/student.module';
import { TeacherPositionModule } from './teacher-position/teacher-position.module';
import { AcademicDegreeModule } from './academic-degree/academic-degree.module';
import { AcademicRankModule } from './academic-rank/academic-rank.module';
import { PlanDisciplineModule } from './plan-discipline/module';
import { TeacherDisciplineModule } from './teacher-discipline/module';
import { GroupModule } from './group/group.module';

@Module({
  imports: [
    DisciplineModule,
    RoleModule,
    UserModule,
    JwtModule,
    AuthModule,
    SpecialtyModule,
    TeacherModule,
    StudentModule,
    TeacherPositionModule,
    AcademicDegreeModule,
    AcademicRankModule,
    PlanDisciplineModule,
    TeacherDisciplineModule,
    GroupModule,
  ],
  controllers: [],
  providers: [],
})
export class CoreModule {}
