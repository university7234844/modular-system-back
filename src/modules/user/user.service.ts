import { Injectable } from '@nestjs/common';

import { DatabaseService } from 'src/database/database.service';
import { defaultPagination, defaultSorting } from 'src/common/constants';
import { ReadAllResult } from 'src/common/types';

import { UserEntity } from './entities';
import { CreateUserDto, ReadAllUserDto } from './dto';

@Injectable()
export class UserService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(createUserDto: CreateUserDto): Promise<UserEntity> {
    const { roles, ...createUserData } = createUserDto;

    const user = await this.prisma.users.create({
      data: {
        ...createUserData,
        roles: {
          create: roles.map((role) => ({
            role: { connect: { id: role.id } },
          })),
        },
      },
      include: {
        roles: {
          select: {
            role: true,
          },
        },
      },
    });

    return user;
  }

  async readAll(
    readAllOptions: ReadAllUserDto,
  ): Promise<ReadAllResult<UserEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const records = await this.prisma.users.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
      include: {
        roles: {
          select: {
            role: true,
          },
        },
        teacher: {
          select: {
            id: true,
            userId: true,
            teacherPositionId: true,
            academicDegreeId: true,
            academicRankId: true,
          },
        },
        student: {
          select: {
            id: true,
            userId: true,
            groupId: true,
          },
        },
      },
    });

    console.log(JSON.stringify(records, null, 2));
    const totalRecordsNumber = await this.prisma.users.count({
      where: { ...filters },
    });

    return { totalRecordsNumber, records };
  }

  async readById(id: number): Promise<UserEntity> {
    const user = await this.prisma.users.findUnique({
      where: { id },
      include: {
        roles: {
          select: {
            role: true,
          },
        },
        teacher: {
          select: {
            id: true,
            userId: true,
            teacherPositionId: true,
            academicDegreeId: true,
            academicRankId: true,
          },
        },
        student: {
          select: {
            id: true,
            userId: true,
            groupId: true,
          },
        },
      },
    });
    return user;
  }

  async readByLogin(login: string): Promise<UserEntity> {
    const user = await this.prisma.users.findUnique({
      where: { login: login },
      include: {
        roles: {
          select: {
            role: true,
          },
        },
        teacher: {
          select: {
            id: true,
            userId: true,
            teacherPositionId: true,
            academicDegreeId: true,
            academicRankId: true,
          },
        },
        student: {
          select: {
            id: true,
            userId: true,
            groupId: true,
          },
        },
      },
    });
    return user;
  }
}
