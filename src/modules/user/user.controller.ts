import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';
import { JwtAuthGuard } from 'src/common/guards';

import { UserService } from './user.service';
import { ReadAllUserDto } from './dto';
import { UserResponse } from './types';
import { RoleEnum } from 'src/common/constants';

@UseGuards(JwtAuthGuard)
@Controller('user')
@ApiTags('User')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  login: { type: 'string' },
                  fullName: { type: 'string' },
                  roles: {
                    type: 'array',
                    items: {
                      type: 'object',
                      properties: {
                        id: { type: 'number' },
                        name: { type: 'string' },
                      },
                    },
                  },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              login: 'SergeyErmochenko',
              fullName: 'Сергей Ермоченко',
              roles: [
                {
                  id: 1,
                  name: RoleEnum.ADMIN,
                },
                {
                  id: 2,
                  name: RoleEnum.TEACHER,
                },
              ],
            },
            {
              id: 2,
              login: 'MikhailHerasimovich',
              fullName: 'Михаил Герасимович',
              roles: [
                {
                  id: 3,
                  name: RoleEnum.STUDENT,
                },
              ],
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllUserDto: ReadAllUserDto,
  ): Promise<ReadAllResult<UserResponse>> {
    const users = await this.userService.readAll(readAllUserDto);
    return {
      totalRecordsNumber: users.totalRecordsNumber,
      records: users.records.map((record) => new UserResponse(record)),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Success', type: UserResponse })
  async readById(@Param('id', ParseIntPipe) id: number): Promise<UserResponse> {
    const user = await this.userService.readById(id);
    return new UserResponse(user);
  }
}
