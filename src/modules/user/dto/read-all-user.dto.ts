import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsOptional, IsString, Validate } from 'class-validator';
import { SexEnum } from 'src/common/constants';

import { BaseReadAllDto } from 'src/common/dto';
import { IsDateFormat } from 'src/common/validators';

export class ReadAllUserDto extends BaseReadAllDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Search User param: "login"',
    example: 'SergeyErmochenko',
  })
  login?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Search User param: "fullName"',
    example: 'Сергей Ермоченко',
  })
  fullName?: string;

  @IsIn([SexEnum.MALE, SexEnum.FEMALE])
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: "User's sex, valid only Male and Female strings",
    example: 'Male',
  })
  sex: string;

  @IsString()
  @IsOptional()
  @Validate(IsDateFormat)
  @ApiProperty({
    required: false,
    description: "User's birthday date (string like YYYY-MM-DD)",
    example: '1980-10-08',
  })
  birthday: string;
}
