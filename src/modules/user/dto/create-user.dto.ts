import { Type } from 'class-transformer';
import {
  IsArray,
  IsIn,
  IsNotEmpty,
  IsString,
  Validate,
  ValidateNested,
} from 'class-validator';

import { SexEnum } from 'src/common/constants';
import { IsDateFormat } from 'src/common/validators';

import { RoleEntity } from 'src/modules/role/entities';
// It's inner dto witch use only for auth
// You shouldn't describe it for swagger

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  login: string;

  @IsString()
  @IsNotEmpty()
  passwordHash: string;

  @IsString()
  @IsNotEmpty()
  fullName: string;

  @IsIn([SexEnum.MALE, SexEnum.FEMALE])
  @IsString()
  @IsNotEmpty()
  sex: string;

  @IsString()
  @Validate(IsDateFormat)
  @IsNotEmpty()
  birthday: string;

  @IsArray({ each: true })
  @ValidateNested({ each: true })
  @Type(() => RoleEntity)
  roles: RoleEntity[];
}
