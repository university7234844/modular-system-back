import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';

import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';

import { UserService } from './user.service';
import { UserController } from './user.controller';
import { JwtStrategy } from 'src/common/strategies';

@Module({
  imports: [DatabaseModule, ConfigModule],
  providers: [
    UserService,
    JwtStrategy,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [UserController],
  exports: [UserService],
})
export class UserModule {}
