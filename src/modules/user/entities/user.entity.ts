import { Users } from '@prisma/client';
import {
  IsArray,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsString,
  Validate,
  ValidateNested,
} from 'class-validator';

import { SexEnum } from 'src/common/constants';
import { IsDateFormat } from 'src/common/validators';
import { RoleEntity } from 'src/modules/role/entities';
import { StudentEntity } from 'src/modules/student/entities';
import { TeacherEntity } from 'src/modules/teacher/entities';

export class UserEntity implements Users {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  login: string;

  @IsString()
  @IsNotEmpty()
  passwordHash: string;

  @IsString()
  @IsNotEmpty()
  fullName: string;

  @IsIn([SexEnum.MALE, SexEnum.FEMALE])
  @IsString()
  @IsNotEmpty()
  sex: string;

  @IsString()
  @IsNotEmpty()
  @Validate(IsDateFormat)
  birthday: string;

  @IsArray()
  roles?: { role: RoleEntity }[];

  @ValidateNested()
  teacher?: TeacherEntity;

  @ValidateNested()
  student?: StudentEntity;
}
