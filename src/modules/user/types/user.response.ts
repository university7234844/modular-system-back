import { ApiProperty } from '@nestjs/swagger';

import { RoleEnum, SexEnum } from 'src/common/constants';
import { TeacherResponse } from 'src/modules/teacher/types/teacher.response';
import { StudentResponse } from 'src/modules/student/types/student.response';

import { UserEntity } from '../entities/user.entity';

export class UserResponse {
  @ApiProperty({ description: "User's id", example: 1 })
  id: number;

  @ApiProperty({ description: "User's login", example: 'SergeyErmochenko' })
  login: string;

  @ApiProperty({ description: "User's full name", example: 'Сергей Ермоченко' })
  fullName: string;

  @ApiProperty({
    description: "User's sex (Male or Female)",
    example: SexEnum.MALE,
  })
  sex: string;

  @ApiProperty({
    description: "User's birthday date (string like YYYY-MM-DD)",
    example: '1980-10-08',
  })
  birthday: string;

  @ApiProperty({
    description: "User's roles",
    type: 'array',
    example: [RoleEnum.ADMIN, RoleEnum.TEACHER],
  })
  roles: string[];

  @ApiProperty({
    description:
      "Depends on the user's role. if the role is a teacher, then there will be an object with the teacher, if a student, then with the student",
    type: TeacherResponse,
  })
  teacher?: TeacherResponse;

  @ApiProperty({
    description:
      "Depends on the user's role. if the role is a teacher, then there will be an object with the teacher, if a student, then with the student",
    type: TeacherResponse,
  })
  student?: StudentResponse;

  constructor(user: UserEntity) {
    this.id = user.id;
    this.login = user.login;
    this.fullName = user.fullName;
    this.sex = user.sex;
    this.birthday = user.birthday;
    this.roles = user?.roles?.map((obj) => obj.role.name);

    if (user.student) {
      this.student = {
        id: user.student.id,
        userId: user.student.userId,
        groupId: user.student.groupId,
      };
    }

    if (user.teacher) {
      this.teacher = {
        ...user.teacher,
      };
    }
  }
}
