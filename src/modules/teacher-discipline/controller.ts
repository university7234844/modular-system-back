import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';

import { ReadAllResult } from 'src/common/types';

import { TeacherDisciplineService } from './service';
import { TeacherDisciplineResponse } from './types';
import {
  CreateTeacherDisciplineDto,
  ReadAllTeacherDisciplineDto,
  UpdateTeacherDisciplineDto,
} from './dto';

@Controller('teacher-discipline')
export class TeacherDisciplineController {
  constructor(private readonly service: TeacherDisciplineService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() createDto: CreateTeacherDisciplineDto,
  ): Promise<TeacherDisciplineResponse> {
    const teacherDiscipline = await this.service.create(createDto);

    return new TeacherDisciplineResponse(teacherDiscipline);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async readAll(
    @Query() readAllOptions: ReadAllTeacherDisciplineDto,
  ): Promise<ReadAllResult<TeacherDisciplineResponse>> {
    const teacherDisciplines = await this.service.readAll(readAllOptions);

    return {
      totalRecordsNumber: teacherDisciplines.totalRecordsNumber,
      records: teacherDisciplines.records.map(
        (record) => new TeacherDisciplineResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<TeacherDisciplineResponse> {
    const teacherDiscipline = await this.service.readById(id);

    return new TeacherDisciplineResponse(teacherDiscipline);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateTeacherDisciplineDto,
  ): Promise<TeacherDisciplineResponse> {
    const teacherDiscipline = await this.service.update(id, updateDto);

    return new TeacherDisciplineResponse(teacherDiscipline);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.service.delete(id);
  }
}
