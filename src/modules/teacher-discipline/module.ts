import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { TeacherDisciplineController } from './controller';
import { TeacherDisciplineService } from './service';

@Module({
  imports: [DatabaseModule],
  controllers: [TeacherDisciplineController],
  providers: [
    TeacherDisciplineService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [TeacherDisciplineService],
})
export class TeacherDisciplineModule {}
