import { TeacherDisciplineEntity } from '../entities';

export class TeacherDisciplineResponse {
  id: number;
  isLector: boolean;
  teacherId: number;
  planDisciplineId: number;

  constructor(entity: TeacherDisciplineEntity) {
    this.id = entity.id;
    this.isLector = entity.isLector;
    this.teacherId = entity.teacherId;
    this.planDisciplineId = entity.planDisciplineId;
  }
}
