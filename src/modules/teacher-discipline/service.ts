import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { ReadAllResult } from 'src/common/types';
import { DatabaseService } from 'src/database/database.service';
import {
  CreateTeacherDisciplineDto,
  ReadAllTeacherDisciplineDto,
  UpdateTeacherDisciplineDto,
} from './dto';
import { TeacherDisciplineEntity } from './entities';

@Injectable()
export class TeacherDisciplineService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createDto: CreateTeacherDisciplineDto,
  ): Promise<TeacherDisciplineEntity> {
    const teacherDiscipline = await this.prisma.teacherDisciplines.create({
      data: createDto,
    });

    return teacherDiscipline;
  }

  async readAll(
    readAllOptions: ReadAllTeacherDisciplineDto,
  ): Promise<ReadAllResult<TeacherDisciplineEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const teacherDiscipline = await this.prisma.teacherDisciplines.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.teacherDisciplines.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: teacherDiscipline,
    };
  }

  async readById(id: number): Promise<TeacherDisciplineEntity> {
    const teacherDiscipline = await this.prisma.teacherDisciplines.findUnique({
      where: { id },
    });

    return teacherDiscipline;
  }

  async update(
    id: number,
    updateDto: UpdateTeacherDisciplineDto,
  ): Promise<TeacherDisciplineEntity> {
    const doesTeacherDisciplineExist = await this.readById(id);

    if (!doesTeacherDisciplineExist) {
      throw new BadRequestException(
        'The specified Teacher Discipline does not exist',
      );
    }

    const teacher = await this.prisma.teacherDisciplines.update({
      where: { id },
      data: { ...updateDto },
    });

    return teacher;
  }

  async delete(id: number): Promise<void> {
    const doesTeacherDisciplineExist = await this.readById(id);

    if (!doesTeacherDisciplineExist) {
      throw new BadRequestException(
        'The specified teacher discipline does not exist',
      );
    }

    await this.prisma.teacherDisciplines.delete({ where: { id } });
  }
}
