import { IsOptional, IsNumber, IsBoolean } from 'class-validator';

import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllTeacherDisciplineDto extends BaseReadAllDto {
  @IsBoolean()
  @IsOptional()
  isLector?: boolean;

  @IsNumber()
  @IsOptional()
  teacherId?: number;

  @IsNumber()
  @IsOptional()
  planDisciplineId?: number;
}
