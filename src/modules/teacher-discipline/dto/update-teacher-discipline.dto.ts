import { IsOptional, IsNumber, IsBoolean } from 'class-validator';

export class UpdateTeacherDisciplineDto {
  @IsBoolean()
  @IsOptional()
  isLector?: boolean;

  @IsNumber()
  @IsOptional()
  teacherId?: number;

  @IsNumber()
  @IsOptional()
  planDisciplineId?: number;
}
