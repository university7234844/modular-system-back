import { IsBoolean, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateTeacherDisciplineDto {
  @IsBoolean()
  @IsNotEmpty()
  isLector: boolean;

  @IsNumber()
  @IsNotEmpty()
  teacherId: number;

  @IsNumber()
  @IsNotEmpty()
  planDisciplineId: number;
}
