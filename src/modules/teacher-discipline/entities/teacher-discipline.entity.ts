import { TeacherDisciplines } from '@prisma/client';
import { IsBoolean, IsNotEmpty, IsNumber } from 'class-validator';

export class TeacherDisciplineEntity implements TeacherDisciplines {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsBoolean()
  @IsNotEmpty()
  isLector: boolean;

  @IsNumber()
  @IsNotEmpty()
  teacherId: number;

  @IsNumber()
  @IsNotEmpty()
  planDisciplineId: number;
}
