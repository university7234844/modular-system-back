import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';

import { AcademicRankService } from './academic-rank.service';
import { AcademicRankController } from './academic-rank.controller';

@Module({
  imports: [DatabaseModule],
  providers: [
    AcademicRankService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [AcademicRankController],
  exports: [AcademicRankService],
})
export class AcademicRankModule {}
