import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { DatabaseService } from 'src/database/database.service';
import { ReadAllResult } from 'src/common/types';

import { AcademicRankEntity } from './entities';
import {
  CreateAcademicRankDto,
  ReadAllAcademicRankDto,
  UpdateAcademicRankDto,
} from './dto';

@Injectable()
export class AcademicRankService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(createDto: CreateAcademicRankDto): Promise<AcademicRankEntity> {
    const existedRank = await this.prisma.academicRanks.findUnique({
      where: { ...createDto },
    });

    if (existedRank) {
      throw new BadRequestException('The specified Rank already exists');
    }

    const rank = await this.prisma.academicRanks.create({
      data: createDto,
    });

    return rank;
  }

  async readAll(
    readAllOptions: ReadAllAcademicRankDto,
  ): Promise<ReadAllResult<AcademicRankEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const ranks = await this.prisma.academicRanks.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.academicRanks.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: ranks,
    };
  }

  async readById(id: number): Promise<AcademicRankEntity> {
    const rank = await this.prisma.academicRanks.findUnique({
      where: { id },
    });

    return rank;
  }

  async update(
    id: number,
    updateDto: UpdateAcademicRankDto,
  ): Promise<AcademicRankEntity> {
    const doesRankExist = await this.readById(id);

    if (!doesRankExist) {
      throw new BadRequestException('The specified Rank does not exist');
    }

    const existedRanks = await this.prisma.academicRanks.findMany({
      where: { ...updateDto },
    });

    if (existedRanks.length) {
      throw new BadRequestException('The specified Rank already exists');
    }

    const rank = await this.prisma.academicRanks.update({
      where: { id },
      data: { ...updateDto },
    });

    return rank;
  }

  async delete(id: number): Promise<void> {
    const doesRankExist = await this.readById(id);

    if (!doesRankExist) {
      throw new BadRequestException('The specified Rank does not exist');
    }

    await this.prisma.academicRanks.delete({ where: { id } });
  }
}
