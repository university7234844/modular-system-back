import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiParam,
} from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';

import { AcademicRankService } from './academic-rank.service';
import { AcademicRankResponse } from './types';
import {
  CreateAcademicRankDto,
  ReadAllAcademicRankDto,
  UpdateAcademicRankDto,
} from './dto';

@Controller('academic-rank')
export class AcademicRankController {
  constructor(private readonly service: AcademicRankService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'Success',
    type: AcademicRankResponse,
  })
  async create(
    @Body() createDto: CreateAcademicRankDto,
  ): Promise<AcademicRankResponse> {
    const Rank = await this.service.create(createDto);

    return new AcademicRankResponse(Rank);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  title: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              title: 'Доцент',
            },
            {
              id: 2,
              title: 'Профессор',
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllAcademicRankDto,
  ): Promise<ReadAllResult<AcademicRankResponse>> {
    const ranks = await this.service.readAll(readAllOptions);

    return {
      totalRecordsNumber: ranks.totalRecordsNumber,
      records: ranks.records.map((record) => new AcademicRankResponse(record)),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get teacher Rank by id',
    type: AcademicRankResponse,
  })
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<AcademicRankResponse> {
    const rank = await this.service.readById(id);

    return new AcademicRankResponse(rank);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Update teacher rank by id',
    type: AcademicRankResponse,
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateAcademicRankDto,
  ): Promise<AcademicRankResponse> {
    const Rank = await this.service.update(id, updateDto);

    return new AcademicRankResponse(Rank);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Delete teacher Rank by id',
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.service.delete(id);
  }
}
