import { AcademicRanks } from '@prisma/client';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AcademicRankEntity implements AcademicRanks {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;
}
