import { ApiProperty } from '@nestjs/swagger';

import { AcademicRankEntity } from '../entities';

export class AcademicRankResponse {
  @ApiProperty({ description: "Rank's id", example: 1 })
  id: number;

  @ApiProperty({
    description: "Rank's title",
    example: 'Кандидат физико-математических наук',
  })
  title: string;

  constructor(rank: AcademicRankEntity) {
    this.id = rank.id;
    this.title = rank.title;
  }
}
