import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllAcademicRankDto extends BaseReadAllDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Rank title',
    example: 'Доцент',
  })
  title?: string;
}
