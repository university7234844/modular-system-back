import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateAcademicRankDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Rank title',
    example: 'Доцент',
  })
  title?: string;
}
