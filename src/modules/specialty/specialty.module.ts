import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { SpecialtyController } from './specialty.controller';
import { SpecialtyService } from './specialty.service';

@Module({
  imports: [DatabaseModule],
  controllers: [SpecialtyController],
  providers: [
    SpecialtyService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [SpecialtyService],
})
export class SpecialtyModule {}
