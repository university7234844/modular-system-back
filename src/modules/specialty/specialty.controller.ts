import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';

import {
  CreateSpecialtyDto,
  ReadAllSpecialtyDto,
  UpdateSpecialtyDto,
} from './dto';
import { SpecialtyResponse } from './types';
import { SpecialtyService } from './specialty.service';

@Controller('specialty')
@ApiTags('Specialty')
export class SpecialtyController {
  constructor(private readonly specialtyService: SpecialtyService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'Success',
    type: SpecialtyResponse,
  })
  async create(
    @Body() createSpecialtyDto: CreateSpecialtyDto,
  ): Promise<SpecialtyResponse> {
    const specialty = await this.specialtyService.create(createSpecialtyDto);

    return new SpecialtyResponse(specialty);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  title: { type: 'string' },
                  shortTitle: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              title: 'Программное обеспечение информационных технологий',
              shortTitle: 'ПОИТ',
            },
            {
              id: 2,
              title: 'УИР',
              shortTitle: 'Управление информационными ресурсами',
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllSpecialtyDto,
  ): Promise<ReadAllResult<SpecialtyResponse>> {
    const specialties = await this.specialtyService.readAll(readAllOptions);

    return {
      totalRecordsNumber: specialties.totalRecordsNumber,
      records: specialties.records.map(
        (record) => new SpecialtyResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get specialty by id',
    type: SpecialtyResponse,
  })
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<SpecialtyResponse> {
    const specialty = await this.specialtyService.readById(id);

    return new SpecialtyResponse(specialty);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Update specialty by id',
    type: SpecialtyResponse,
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateSpecialtyDto: UpdateSpecialtyDto,
  ): Promise<SpecialtyResponse> {
    const specialty = await this.specialtyService.update(
      id,
      updateSpecialtyDto,
    );

    return new SpecialtyResponse(specialty);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Delete specialty by id',
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.specialtyService.delete(id);
  }
}
