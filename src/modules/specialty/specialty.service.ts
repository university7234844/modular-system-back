import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { ReadAllResult } from 'src/common/types';
import { DatabaseService } from 'src/database/database.service';

import {
  CreateSpecialtyDto,
  ReadAllSpecialtyDto,
  UpdateSpecialtyDto,
} from './dto';
import { SpecialtyEntity } from './entities';

@Injectable()
export class SpecialtyService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createSpecialtyDto: CreateSpecialtyDto,
  ): Promise<SpecialtyEntity> {
    const existedSpecialty = await this.prisma.specialties.findUnique({
      where: { ...createSpecialtyDto },
    });

    if (existedSpecialty) {
      throw new BadRequestException('The specified specialty already exists');
    }

    const specialty = await this.prisma.specialties.create({
      data: createSpecialtyDto,
    });

    return specialty;
  }

  async readAll(
    readAllOptions: ReadAllSpecialtyDto,
  ): Promise<ReadAllResult<SpecialtyEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const specialties = await this.prisma.specialties.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.specialties.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: specialties,
    };
  }

  async readById(id: number): Promise<SpecialtyEntity> {
    const specialty = await this.prisma.specialties.findUnique({
      where: { id },
    });

    return specialty;
  }

  async update(
    id: number,
    updateSpecialtyDto: UpdateSpecialtyDto,
  ): Promise<SpecialtyEntity> {
    const doesSpecialtyExist = await this.readById(id);

    if (!doesSpecialtyExist) {
      throw new BadRequestException('The specified specialty does not exist');
    }

    const existedSpecialty = await this.prisma.disciplines.findMany({
      where: { ...updateSpecialtyDto },
    });

    if (existedSpecialty.length) {
      throw new BadRequestException('The specified specialty already exists');
    }

    const specialty = await this.prisma.specialties.update({
      where: { id },
      data: { ...updateSpecialtyDto },
    });

    return specialty;
  }

  async delete(id: number): Promise<void> {
    const doesSpecialtyExist = await this.readById(id);

    if (!doesSpecialtyExist) {
      throw new BadRequestException('The specified specialty does not exist');
    }

    await this.prisma.specialties.delete({ where: { id } });
  }
}
