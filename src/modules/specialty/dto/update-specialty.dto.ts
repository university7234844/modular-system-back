import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateSpecialtyDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Specialty title',
    example: 'Программное обеспечение информационных технологий',
  })
  title?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Specialty short title',
    example: 'ПОИТ',
  })
  shortTitle?: string;
}
