import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateSpecialtyDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: 'Specialty title',
    example: 'Программное обеспечение информационных технологий',
  })
  title: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    required: true,
    description: 'Specialty short title',
    example: 'ПОИТ',
  })
  shortTitle: string;
}
