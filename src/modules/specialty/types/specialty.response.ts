import { ApiProperty, ApiTags } from '@nestjs/swagger';

import { SpecialtyEntity } from '../entities';

@ApiTags('Specialty')
export class SpecialtyResponse {
  @ApiProperty({ description: "Specialty's id", example: 1 })
  id: number;

  @ApiProperty({
    description: "Specialty's title",
    example: 'Программное обеспечение информационных технологий',
  })
  title: string;

  @ApiProperty({
    description: "Specialty's short title",
    example: 'ПОИТ',
  })
  shortTitle: string;

  constructor(specialty: SpecialtyEntity) {
    this.id = specialty.id;
    this.title = specialty.title;
    this.shortTitle = specialty.shortTitle;
  }
}
