import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import type { Specialties } from '@prisma/client';

export class SpecialtyEntity implements Specialties {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  shortTitle: string;
}
