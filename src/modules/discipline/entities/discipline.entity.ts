import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import type { Disciplines } from '@prisma/client';

export class DisciplineEntity implements Disciplines {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  shortTitle: string;
}
