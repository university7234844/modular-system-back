import { BadRequestException, Injectable } from '@nestjs/common';
import { DatabaseService } from 'src/database/database.service';

import { ReadAllResult } from 'src/common/types';
import { defaultPagination, defaultSorting } from 'src/common/constants';

import {
  CreateDisciplineDto,
  ReadAllDisciplineDto,
  UpdateDisciplineDto,
} from './dto';
import { DisciplineEntity } from './entities';

@Injectable()
export class DisciplineService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createDisciplineDto: CreateDisciplineDto,
  ): Promise<DisciplineEntity> {
    const existedDiscipline = await this.prisma.disciplines.findUnique({
      where: { ...createDisciplineDto },
    });

    if (existedDiscipline) {
      throw new BadRequestException('The specified discipline already exists');
    }

    const discipline = await this.prisma.disciplines.create({
      data: createDisciplineDto,
    });

    return discipline;
  }

  async readAll(
    readAllOptions: ReadAllDisciplineDto,
  ): Promise<ReadAllResult<DisciplineEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const disciplines = await this.prisma.disciplines.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.disciplines.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: disciplines,
    };
  }

  async readById(id: number): Promise<DisciplineEntity> {
    const discipline = await this.prisma.disciplines.findUnique({
      where: { id },
    });

    return discipline;
  }

  async update(
    id: number,
    updateDisciplineDto: UpdateDisciplineDto,
  ): Promise<DisciplineEntity> {
    const doesDisciplineExist = await this.readById(id);

    if (!doesDisciplineExist) {
      throw new BadRequestException('The specified discipline does not exist');
    }

    const existedDiscipline = await this.prisma.disciplines.findMany({
      where: { ...updateDisciplineDto },
    });

    if (existedDiscipline.length) {
      throw new BadRequestException('The specified discipline already exists');
    }

    const discipline = await this.prisma.disciplines.update({
      where: { id },
      data: { ...updateDisciplineDto },
    });

    return discipline;
  }

  async delete(id: number): Promise<void> {
    const doesDisciplineExist = await this.readById(id);

    if (!doesDisciplineExist) {
      throw new BadRequestException('The specified discipline does not exist');
    }

    await this.prisma.disciplines.delete({ where: { id } });
  }
}
