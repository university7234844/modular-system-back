import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateDisciplineDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Discipline title',
    example: 'Компьютерные системы и сети',
  })
  title?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Discipline short title',
    example: 'КСИС',
  })
  shortTitle?: string;
}
