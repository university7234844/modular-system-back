import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllDisciplineDto extends BaseReadAllDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Search Discipline param: "title"',
    example: 'Компьютерные системы и сети',
  })
  title?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Search Discipline param: "shortTitle"',
    example: 'КСИС',
  })
  shortTitle?: string;
}
