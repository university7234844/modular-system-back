import { ApiProperty, ApiTags } from '@nestjs/swagger';

import { DisciplineEntity } from '../entities';

@ApiTags('Discipline')
export class DisciplineResponse {
  @ApiProperty({ description: "Discipline's id", example: 1 })
  id: number;

  @ApiProperty({
    description: "Discipline's title",
    example: 'Компьютерные системы и сети',
  })
  title: string;

  @ApiProperty({
    description: "Discipline's short title",
    example: 'КСИС',
  })
  shortTitle: string;

  constructor(discipline: DisciplineEntity) {
    this.id = discipline.id;
    this.title = discipline.title;
    this.shortTitle = discipline.shortTitle;
  }
}
