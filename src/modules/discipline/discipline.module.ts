import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { DisciplineController } from './discipline.controller';
import { DisciplineService } from './discipline.service';

@Module({
  imports: [DatabaseModule],
  controllers: [DisciplineController],
  providers: [
    DisciplineService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [DisciplineService],
})
export class DisciplineModule {}
