import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';

import {
  CreateDisciplineDto,
  ReadAllDisciplineDto,
  UpdateDisciplineDto,
} from './dto';
import { DisciplineResponse } from './types';
import { DisciplineService } from './discipline.service';

@Controller('discipline')
@ApiTags('Discipline')
export class DisciplineController {
  constructor(private readonly disciplineService: DisciplineService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'Success',
    type: DisciplineResponse,
  })
  async create(
    @Body() createDisciplineDto: CreateDisciplineDto,
  ): Promise<DisciplineResponse> {
    const discipline = await this.disciplineService.create(createDisciplineDto);

    return new DisciplineResponse(discipline);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  title: { type: 'string' },
                  shortTitle: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              title: 'Компьютерные системы и сети',
              shortTitle: 'КСИС',
            },
            {
              id: 2,
              title: 'ОООбъектно ориентированное программированиеП',
              shortTitle: 'ООП',
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllDisciplineDto,
  ): Promise<ReadAllResult<DisciplineResponse>> {
    const disciplines = await this.disciplineService.readAll(readAllOptions);

    return {
      totalRecordsNumber: disciplines.totalRecordsNumber,
      records: disciplines.records.map(
        (record) => new DisciplineResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get discipline by id',
    type: DisciplineResponse,
  })
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DisciplineResponse> {
    const discipline = await this.disciplineService.readById(id);

    return new DisciplineResponse(discipline);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Update discipline by id',
    type: DisciplineResponse,
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDisciplineDto: UpdateDisciplineDto,
  ): Promise<DisciplineResponse> {
    const discipline = await this.disciplineService.update(
      id,
      updateDisciplineDto,
    );

    return new DisciplineResponse(discipline);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Delete discipline by id',
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.disciplineService.delete(id);
  }
}
