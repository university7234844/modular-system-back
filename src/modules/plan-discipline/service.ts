import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { ReadAllResult } from 'src/common/types';
import { DatabaseService } from 'src/database/database.service';
import {
  CreatePlanDisciplineDto,
  ReadAllPlanDisciplineDto,
  UpdatePlanDisciplineDto,
} from './dto';
import { PlanDisciplineEntity } from './entities';

@Injectable()
export class PlanDisciplineService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createDto: CreatePlanDisciplineDto,
  ): Promise<PlanDisciplineEntity> {
    const plan = await this.prisma.planDisciplines.create({
      data: createDto,
    });

    return plan;
  }

  async readAll(
    readAllOptions: ReadAllPlanDisciplineDto,
  ): Promise<ReadAllResult<PlanDisciplineEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const plan = await this.prisma.planDisciplines.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.planDisciplines.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: plan,
    };
  }

  async readById(id: number): Promise<PlanDisciplineEntity> {
    const plan = await this.prisma.planDisciplines.findUnique({
      where: { id },
    });

    return plan;
  }

  async update(
    id: number,
    updateDto: UpdatePlanDisciplineDto,
  ): Promise<PlanDisciplineEntity> {
    const doesPlanExist = await this.readById(id);

    if (!doesPlanExist) {
      throw new BadRequestException('The specified plan does not exist');
    }

    const plan = await this.prisma.planDisciplines.update({
      where: { id },
      data: { ...updateDto },
    });

    return plan;
  }

  async delete(id: number): Promise<void> {
    const doesPlanExist = await this.readById(id);

    if (!doesPlanExist) {
      throw new BadRequestException('The specified plan does not exist');
    }

    await this.prisma.planDisciplines.delete({ where: { id } });
  }
}
