import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';

import { ReadAllResult } from 'src/common/types';

import { PlanDisciplineService } from './service';
import { PlanDisciplineResponse } from './types';
import {
  CreatePlanDisciplineDto,
  ReadAllPlanDisciplineDto,
  UpdatePlanDisciplineDto,
} from './dto';

@Controller('plan-discipline')
export class PlanDisciplineController {
  constructor(private readonly service: PlanDisciplineService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(
    @Body() createDto: CreatePlanDisciplineDto,
  ): Promise<PlanDisciplineResponse> {
    const plan = await this.service.create(createDto);

    return new PlanDisciplineResponse(plan);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async readAll(
    @Query() readAllOptions: ReadAllPlanDisciplineDto,
  ): Promise<ReadAllResult<PlanDisciplineResponse>> {
    const plans = await this.service.readAll(readAllOptions);

    return {
      totalRecordsNumber: plans.totalRecordsNumber,
      records: plans.records.map(
        (record) => new PlanDisciplineResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<PlanDisciplineResponse> {
    const plan = await this.service.readById(id);

    return new PlanDisciplineResponse(plan);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdatePlanDisciplineDto,
  ): Promise<PlanDisciplineResponse> {
    const plan = await this.service.update(id, updateDto);

    return new PlanDisciplineResponse(plan);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.service.delete(id);
  }
}
