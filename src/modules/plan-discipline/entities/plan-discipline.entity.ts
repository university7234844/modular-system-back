import { PlanDisciplines } from '@prisma/client';
import { IsIn, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ControlFormEnum } from 'src/common/constants';

const { EXAM, CREDIT_TEST, DIFFERENTIATED_CREDIT_TEST } = ControlFormEnum;

export class PlanDisciplineEntity implements PlanDisciplines {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsNumber()
  @IsNotEmpty()
  lectureHoursNumber: number;

  @IsNumber()
  @IsNotEmpty()
  practicalHoursNumber: number;

  @IsNumber()
  @IsNotEmpty()
  laboratoryHoursNumber: number;

  @IsNumber()
  @IsNotEmpty()
  kusrHoursNumber: number;

  @IsNumber()
  @IsNotEmpty()
  controlWorkNumber: number;

  @IsString()
  @IsNotEmpty()
  @IsIn([EXAM, CREDIT_TEST, DIFFERENTIATED_CREDIT_TEST])
  controlForm: string;

  @IsNumber()
  @IsNotEmpty()
  disciplineId: number;

  @IsNumber()
  @IsNotEmpty()
  groupId: number;
}
