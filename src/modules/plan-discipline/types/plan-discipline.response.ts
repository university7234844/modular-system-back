import { PlanDisciplineEntity } from '../entities';

export class PlanDisciplineResponse {
  id: number;
  lectureHoursNumber: number;
  practicalHoursNumber: number;
  laboratoryHoursNumber: number;
  kusrHoursNumber: number;
  controlWorkNumber: number;
  controlForm: string;
  disciplineId: number;
  groupId: number;

  constructor(entity: PlanDisciplineEntity) {
    this.id = entity.id;
    this.lectureHoursNumber = entity.lectureHoursNumber;
    this.practicalHoursNumber = entity.practicalHoursNumber;
    this.laboratoryHoursNumber = entity.laboratoryHoursNumber;
    this.kusrHoursNumber = entity.kusrHoursNumber;
    this.controlWorkNumber = entity.controlWorkNumber;
    this.controlForm = entity.controlForm;
    this.disciplineId = entity.disciplineId;
    this.groupId = entity.groupId;
  }
}
