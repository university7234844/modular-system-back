import { IsIn, IsOptional, IsNumber, IsString } from 'class-validator';

import { ControlFormEnum } from 'src/common/constants';

const { EXAM, CREDIT_TEST, DIFFERENTIATED_CREDIT_TEST } = ControlFormEnum;

export class UpdatePlanDisciplineDto {
  @IsNumber()
  @IsOptional()
  lectureHoursNumber?: number;

  @IsNumber()
  @IsOptional()
  practicalHoursNumber?: number;

  @IsNumber()
  @IsOptional()
  laboratoryHoursNumber?: number;

  @IsNumber()
  @IsOptional()
  kusrHoursNumber?: number;

  @IsNumber()
  @IsOptional()
  controlWorkNumber?: number;

  @IsString()
  @IsOptional()
  @IsIn([EXAM, CREDIT_TEST, DIFFERENTIATED_CREDIT_TEST])
  controlForm?: string;

  @IsNumber()
  @IsOptional()
  disciplineId?: number;

  @IsNumber()
  @IsOptional()
  groupId?: number;
}
