import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { PlanDisciplineController } from './controller';
import { PlanDisciplineService } from './service';

@Module({
  imports: [DatabaseModule],
  controllers: [PlanDisciplineController],
  providers: [
    PlanDisciplineService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [PlanDisciplineService],
})
export class PlanDisciplineModule {}
