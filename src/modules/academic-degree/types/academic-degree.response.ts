import { ApiProperty } from '@nestjs/swagger';

import { AcademicDegreeEntity } from '../entities';

export class AcademicDegreeResponse {
  @ApiProperty({ description: "Degree's id", example: 1 })
  id: number;

  @ApiProperty({
    description: "Degree's title",
    example: 'Кандидат физико-математических наук',
  })
  title: string;

  constructor(degree: AcademicDegreeEntity) {
    this.id = degree.id;
    this.title = degree.title;
  }
}
