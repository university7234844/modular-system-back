import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';

import { AcademicDegreeService } from './academic-degree.service';
import { AcademicDegreeController } from './academic-degree.controller';

@Module({
  imports: [DatabaseModule],
  providers: [
    AcademicDegreeService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [AcademicDegreeController],
  exports: [AcademicDegreeService],
})
export class AcademicDegreeModule {}
