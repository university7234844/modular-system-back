import { AcademicDegrees } from '@prisma/client';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AcademicDegreeEntity implements AcademicDegrees {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;
}
