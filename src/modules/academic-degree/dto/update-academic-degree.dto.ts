import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class UpdateAcademicDegreeDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Degree title',
    example: 'Кандидат физико-математических наук',
  })
  title?: string;
}
