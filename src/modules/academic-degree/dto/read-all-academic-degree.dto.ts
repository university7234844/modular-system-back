import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllAcademicDegreeDto extends BaseReadAllDto {
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Degree title',
    example: 'Кандидат физико-математических наук',
  })
  title?: string;
}
