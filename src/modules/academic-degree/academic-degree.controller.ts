import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiParam,
} from '@nestjs/swagger';

import { ReadAllResult } from 'src/common/types';

import { AcademicDegreeService } from './academic-degree.service';
import { AcademicDegreeResponse } from './types';
import {
  CreateAcademicDegreeDto,
  ReadAllAcademicDegreeDto,
  UpdateAcademicDegreeDto,
} from './dto';

@Controller('academic-degree')
export class AcademicDegreeController {
  constructor(private readonly service: AcademicDegreeService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    description: 'Success',
    type: AcademicDegreeResponse,
  })
  async create(
    @Body() createDto: CreateAcademicDegreeDto,
  ): Promise<AcademicDegreeResponse> {
    const degree = await this.service.create(createDto);

    return new AcademicDegreeResponse(degree);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  title: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 2,
          records: [
            {
              id: 1,
              title: 'Кандидат физико-математических наук',
            },
            {
              id: 2,
              title: 'Профессор физико-математических наук',
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllAcademicDegreeDto,
  ): Promise<ReadAllResult<AcademicDegreeResponse>> {
    const degrees = await this.service.readAll(readAllOptions);

    return {
      totalRecordsNumber: degrees.totalRecordsNumber,
      records: degrees.records.map(
        (record) => new AcademicDegreeResponse(record),
      ),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get teacher degree by id',
    type: AcademicDegreeResponse,
  })
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<AcademicDegreeResponse> {
    const degree = await this.service.readById(id);

    return new AcademicDegreeResponse(degree);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Update teacher degree by id',
    type: AcademicDegreeResponse,
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateAcademicDegreeDto,
  ): Promise<AcademicDegreeResponse> {
    const degree = await this.service.update(id, updateDto);

    return new AcademicDegreeResponse(degree);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Delete teacher degree by id',
  })
  @ApiParam({ name: 'id', type: 'number', example: 1 })
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.service.delete(id);
  }
}
