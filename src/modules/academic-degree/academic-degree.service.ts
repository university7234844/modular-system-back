import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { DatabaseService } from 'src/database/database.service';
import { ReadAllResult } from 'src/common/types';

import { AcademicDegreeEntity } from './entities';
import {
  CreateAcademicDegreeDto,
  ReadAllAcademicDegreeDto,
  UpdateAcademicDegreeDto,
} from './dto';

@Injectable()
export class AcademicDegreeService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(
    createDto: CreateAcademicDegreeDto,
  ): Promise<AcademicDegreeEntity> {
    const existedDegree = await this.prisma.academicDegrees.findUnique({
      where: { ...createDto },
    });

    if (existedDegree) {
      throw new BadRequestException('The specified degree already exists');
    }

    const degree = await this.prisma.academicDegrees.create({
      data: createDto,
    });

    return degree;
  }

  async readAll(
    readAllOptions: ReadAllAcademicDegreeDto,
  ): Promise<ReadAllResult<AcademicDegreeEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const degrees = await this.prisma.academicDegrees.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.academicDegrees.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: degrees,
    };
  }

  async readById(id: number): Promise<AcademicDegreeEntity> {
    const degree = await this.prisma.academicDegrees.findUnique({
      where: { id },
    });

    return degree;
  }

  async update(
    id: number,
    updateDto: UpdateAcademicDegreeDto,
  ): Promise<AcademicDegreeEntity> {
    const doesDegreeExist = await this.readById(id);

    if (!doesDegreeExist) {
      throw new BadRequestException('The specified degree does not exist');
    }

    const existedDegrees = await this.prisma.academicDegrees.findMany({
      where: { ...updateDto },
    });

    if (existedDegrees.length) {
      throw new BadRequestException('The specified degree already exists');
    }

    const degree = await this.prisma.academicDegrees.update({
      where: { id },
      data: { ...updateDto },
    });

    return degree;
  }

  async delete(id: number): Promise<void> {
    const doesDegreeExist = await this.readById(id);

    if (!doesDegreeExist) {
      throw new BadRequestException('The specified degree does not exist');
    }

    await this.prisma.academicDegrees.delete({ where: { id } });
  }
}
