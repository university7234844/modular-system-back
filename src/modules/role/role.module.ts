import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';
import { JwtStrategy } from 'src/common/strategies';

import { RoleService } from './role.service';
import { RoleController } from './role.controller';

@Module({
  imports: [DatabaseModule, ConfigModule],
  providers: [
    RoleService,
    JwtStrategy,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [RoleController],
  exports: [RoleService],
})
export class RoleModule {}
