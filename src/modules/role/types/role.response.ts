import { ApiProperty } from '@nestjs/swagger';

import { RoleEntity } from '../entities/role.entity';
import { RoleEnum } from 'src/common/constants';

export class RoleResponse {
  @ApiProperty({ description: "Role's id", example: 1 })
  id: number;

  @ApiProperty({ description: "Role's name", example: RoleEnum.ADMIN })
  name: string;

  constructor(role: RoleEntity) {
    this.id = role.id;
    this.name = role.name;
  }
}
