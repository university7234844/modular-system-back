import { ApiProperty } from '@nestjs/swagger';
import { Roles } from '@prisma/client';
import { IsIn, IsNotEmpty, IsNumber, IsString } from 'class-validator';

import { RoleEnum } from 'src/common/constants';

export class RoleEntity implements Roles {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ description: "Role's id", example: 1 })
  id: number;

  @IsIn([RoleEnum.ADMIN, RoleEnum.TEACHER, RoleEnum.STUDENT])
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: "Role's name", example: RoleEnum.STUDENT })
  name: string;
}
