import { Injectable } from '@nestjs/common';

import { DatabaseService } from 'src/database/database.service';
import { ReadAllResult } from 'src/common/types';
import { defaultPagination, defaultSorting } from 'src/common/constants';

import { RoleEntity } from './entities';
import { ReadAllRoleDto } from './dto';

@Injectable()
export class RoleService {
  constructor(private readonly prisma: DatabaseService) {}

  async readAll(
    readAllOptions: ReadAllRoleDto,
  ): Promise<ReadAllResult<RoleEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const records = await this.prisma.roles.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const totalRecordsNumber = await this.prisma.roles.count({
      where: { ...filters },
    });

    return { totalRecordsNumber, records };
  }

  async readById(id: number): Promise<RoleEntity> {
    const role = await this.prisma.roles.findUnique({
      where: { id },
    });
    return role;
  }

  async readAllByName(names: string[]): Promise<RoleEntity[]> {
    const roles = await this.prisma.roles.findMany({
      where: { OR: names.map((name) => ({ name })) },
    });
    return roles;
  }

  async readAllById(ids: number[]): Promise<RoleEntity[]> {
    const roles = await this.prisma.roles.findMany({
      where: { OR: ids.map((id) => ({ id })) },
    });
    return roles;
  }
}
