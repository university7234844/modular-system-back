import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';

import { ReadAllResult } from 'src/common/types';
import { JwtAuthGuard, RolesGuard } from 'src/common/guards';
import { Roles } from 'src/common/decorators';
import { RoleEnum } from 'src/common/constants';

import { RoleService } from './role.service';
import { ReadAllRoleDto } from './dto';
import { RoleResponse } from './types';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

@UseGuards(JwtAuthGuard, RolesGuard)
@Roles(RoleEnum.ADMIN)
@Controller('role')
@ApiTags('Role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Success',
    content: {
      'application/json': {
        schema: {
          type: 'object',
          properties: {
            totalRecordsNumber: { type: 'number' },
            records: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: { type: 'number' },
                  name: { type: 'string' },
                },
              },
            },
          },
        },
        example: {
          totalRecordsNumber: 3,
          records: [
            {
              id: 1,
              name: RoleEnum.ADMIN,
            },
            {
              id: 2,
              name: RoleEnum.TEACHER,
            },
            {
              id: 3,
              name: RoleEnum.STUDENT,
            },
          ],
        },
      },
    },
  })
  async readAll(
    @Query() readAllOptions: ReadAllRoleDto,
  ): Promise<ReadAllResult<RoleResponse>> {
    const roles = await this.roleService.readAll(readAllOptions);
    return {
      totalRecordsNumber: roles.totalRecordsNumber,
      records: roles.records.map((record) => new RoleResponse(record)),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ description: 'Success', type: RoleResponse })
  async readById(@Param('id', ParseIntPipe) id: number): Promise<RoleResponse> {
    const role = await this.roleService.readById(id);
    return new RoleResponse(role);
  }
}
