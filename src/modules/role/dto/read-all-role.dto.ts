import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsOptional, IsString } from 'class-validator';

import { RoleEnum } from 'src/common/constants';
import { BaseReadAllDto } from 'src/common/dto';

export class ReadAllRoleDto extends BaseReadAllDto {
  @IsIn([RoleEnum.ADMIN, RoleEnum.TEACHER, RoleEnum.STUDENT])
  @IsString()
  @IsOptional()
  @ApiProperty({
    required: false,
    description: 'Search Role param: "name"',
    example: RoleEnum.ADMIN,
  })
  name?: string;
}
