import { BadRequestException, Injectable } from '@nestjs/common';

import { defaultPagination, defaultSorting } from 'src/common/constants';
import { ReadAllResult } from 'src/common/types';
import { DatabaseService } from 'src/database/database.service';

import { CreateGroupDto, ReadAllGroupDto, UpdateGroupDto } from './dto';
import { GroupEntity } from './entities';

@Injectable()
export class GroupService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(createGroupDto: CreateGroupDto): Promise<GroupEntity> {
    const existedGroup = await this.prisma.groups.findUnique({
      where: { ...createGroupDto },
    });

    if (existedGroup) {
      throw new BadRequestException('The specified group already exists');
    }

    const group = await this.prisma.groups.create({
      data: createGroupDto,
    });

    return group;
  }

  async readAll(
    readAllOptions: ReadAllGroupDto,
  ): Promise<ReadAllResult<GroupEntity>> {
    const { sorting, pagination, ...filters } = readAllOptions;

    const { column, direction } = sorting ?? defaultSorting;
    const { offset, size } = pagination ?? defaultPagination;

    const groups = await this.prisma.groups.findMany({
      where: { ...filters },
      orderBy: { [column]: direction },
      skip: offset,
      take: Number(size),
    });

    const count = await this.prisma.groups.count({
      where: { ...filters },
    });

    return {
      totalRecordsNumber: count,
      records: groups,
    };
  }

  async readById(id: number): Promise<GroupEntity> {
    const group = await this.prisma.groups.findUnique({
      where: { id },
    });

    return group;
  }

  async update(
    id: number,
    updateGroupDto: UpdateGroupDto,
  ): Promise<GroupEntity> {
    const doesGroupExist = await this.readById(id);

    if (!doesGroupExist) {
      throw new BadRequestException('The specified group does not exist');
    }

    const existedGroup = await this.prisma.disciplines.findMany({
      where: { ...updateGroupDto },
    });

    if (existedGroup.length) {
      throw new BadRequestException('The specified group already exists');
    }

    const group = await this.prisma.groups.update({
      where: { id },
      data: { ...updateGroupDto },
    });

    return group;
  }

  async delete(id: number): Promise<void> {
    const doesGroupExist = await this.readById(id);

    if (!doesGroupExist) {
      throw new BadRequestException('The specified group does not exist');
    }

    await this.prisma.groups.delete({ where: { id } });
  }
}
