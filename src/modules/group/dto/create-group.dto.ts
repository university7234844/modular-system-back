import { IsIn, IsNotEmpty, IsNumber, IsString } from 'class-validator';

import { EducationalFormEnum } from 'src/common/constants';

const { FULL_TIME, PART_TIME } = EducationalFormEnum;

export class CreateGroupDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsNumber()
  @IsNotEmpty()
  recruitmentYear: number;

  @IsString()
  @IsNotEmpty()
  @IsIn([FULL_TIME, PART_TIME])
  educationalForm: string;

  @IsNumber()
  @IsNotEmpty()
  specialtyId: number;
}
