import { IsIn, IsNumber, IsOptional, IsString } from 'class-validator';

import { EducationalFormEnum } from 'src/common/constants';

const { FULL_TIME, PART_TIME } = EducationalFormEnum;

export class UpdateGroupDto {
  @IsString()
  @IsOptional()
  title?: string;

  @IsNumber()
  @IsOptional()
  recruitmentYear?: number;

  @IsString()
  @IsOptional()
  @IsIn([FULL_TIME, PART_TIME])
  educationalForm?: string;

  @IsNumber()
  @IsOptional()
  specialtyId?: number;
}
