import { IsIn, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import type { Groups } from '@prisma/client';

import { EducationalFormEnum } from 'src/common/constants';

const { FULL_TIME, PART_TIME } = EducationalFormEnum;

export class GroupEntity implements Groups {
  @IsNumber()
  @IsNotEmpty()
  id: number;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsNumber()
  @IsNotEmpty()
  recruitmentYear: number;

  @IsString()
  @IsNotEmpty()
  @IsIn([FULL_TIME, PART_TIME])
  educationalForm: string;

  @IsNumber()
  @IsNotEmpty()
  specialtyId: number;
}
