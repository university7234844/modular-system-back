import { GroupEntity } from '../entities';

export class GroupResponse {
  id: number;
  title: string;
  recruitmentYear: number;
  educationalForm: string;
  specialtyId: number;

  constructor(group: GroupEntity) {
    this.id = group.id;
    this.title = group.title;
    this.recruitmentYear = group.recruitmentYear;
    this.educationalForm = group.educationalForm;
    this.specialtyId = group.specialtyId;
  }
}
