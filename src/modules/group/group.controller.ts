import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Delete,
  Query,
  ParseIntPipe,
} from '@nestjs/common';

import { ReadAllResult } from 'src/common/types';

import { CreateGroupDto, ReadAllGroupDto, UpdateGroupDto } from './dto';
import { GroupResponse } from './types';
import { GroupService } from './group.service';

@Controller('specialty')
export class GroupController {
  constructor(private readonly specialtyService: GroupService) {}

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async create(@Body() createGroupDto: CreateGroupDto): Promise<GroupResponse> {
    const specialty = await this.specialtyService.create(createGroupDto);

    return new GroupResponse(specialty);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async readAll(
    @Query() readAllOptions: ReadAllGroupDto,
  ): Promise<ReadAllResult<GroupResponse>> {
    const groups = await this.specialtyService.readAll(readAllOptions);

    return {
      totalRecordsNumber: groups.totalRecordsNumber,
      records: groups.records.map((record) => new GroupResponse(record)),
    };
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async readById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<GroupResponse> {
    const specialty = await this.specialtyService.readById(id);

    return new GroupResponse(specialty);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<GroupResponse> {
    const specialty = await this.specialtyService.update(id, updateGroupDto);

    return new GroupResponse(specialty);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.specialtyService.delete(id);
  }
}
