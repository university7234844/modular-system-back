import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { DatabaseModule } from 'src/database/database.module';
import { AllExceptionsFilter } from 'src/common/filters';

import { GroupController } from './group.controller';
import { GroupService } from './group.service';

@Module({
  imports: [DatabaseModule],
  controllers: [GroupController],
  providers: [
    GroupService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  exports: [GroupService],
})
export class GroupModule {}
