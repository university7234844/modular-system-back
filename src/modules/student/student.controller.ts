import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Put,
} from '@nestjs/common';
import { StudentService } from './student.service';
import { UpdateStudentDto } from './dto';
import { StudentResponse } from './types';

@Controller('student')
export class StudentController {
  constructor(private readonly service: StudentService) {}

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateDto: UpdateStudentDto,
  ): Promise<StudentResponse> {
    const student = await this.service.update(id, updateDto);
    return student;
  }
}
