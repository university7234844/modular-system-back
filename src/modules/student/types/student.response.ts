import { StudentEntity } from '../entities';

export class StudentResponse {
  id: number;
  userId: number;
  groupId: number | null;

  constructor(student: StudentEntity) {
    this.id = student.id;
    this.userId = student.userId;
    this.groupId = student.groupId;
  }
}
