import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { AllExceptionsFilter } from 'src/common/filters';
import { DatabaseModule } from 'src/database/database.module';

import { StudentService } from './student.service';
import { StudentController } from './student.controller';

@Module({
  imports: [DatabaseModule],
  providers: [
    StudentService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
  controllers: [StudentController],
  exports: [StudentService],
})
export class StudentModule {}
