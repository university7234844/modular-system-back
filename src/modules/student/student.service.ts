import { BadRequestException, Injectable } from '@nestjs/common';

import { DatabaseService } from 'src/database/database.service';

import { CreateStudentDto, UpdateStudentDto } from './dto';
import { StudentEntity } from './entities/';

@Injectable()
export class StudentService {
  constructor(private readonly prisma: DatabaseService) {}

  async create(createStudentDto: CreateStudentDto): Promise<StudentEntity> {
    const { userId } = createStudentDto;

    const existedStudent = await this.prisma.students.findFirst({
      where: { userId },
    });

    if (existedStudent) {
      throw new BadRequestException(
        `Student for User with UserId=${userId} already exist`,
      );
    }

    const student = await this.prisma.students.create({ data: { userId } });

    return student;
  }

  async update(
    id: number,
    updateDto: UpdateStudentDto,
  ): Promise<StudentEntity> {
    const student = await this.prisma.students.update({
      where: { id },
      data: { ...updateDto },
    });

    return student;
  }
}
