import { IsNumber, IsOptional } from 'class-validator';

export class UpdateStudentDto {
  @IsNumber()
  @IsOptional()
  groupId?: number;
}
