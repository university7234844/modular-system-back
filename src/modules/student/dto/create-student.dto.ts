import { IsNotEmpty, IsNumber } from 'class-validator';

export class CreateStudentDto {
  @IsNumber()
  @IsNotEmpty()
  userId: number;
}
