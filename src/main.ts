import { config } from 'dotenv';
config();

import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import * as cookieParser from 'cookie-parser';
import * as fs from 'fs/promises';

import { AppModule } from './app.module';
import { CookieEnum } from './common/constants';

async function bootstrap() {
  const httpsOptions = {
    key: await fs.readFile('./https/cert.key'),
    cert: await fs.readFile('./https/cert.crt'),
  };

  const app = await NestFactory.create(AppModule, { httpsOptions });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );

  app.use(cookieParser());
  app.enableCors({
    credentials: true,
    origin: 'https://localhost:5173',
  });

  const config = new DocumentBuilder()
    .setTitle('Modular System API')
    .setDescription(
      'API for the application of student performance monitoring and knowledge base management for laboratory work',
    )
    .setVersion('1.0')
    .addCookieAuth(CookieEnum.AUTH, {
      type: 'apiKey',
      in: 'cookie',
      name: CookieEnum.AUTH,
    })
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('doc', app, document);

  await app.listen(3000);
}
bootstrap();
