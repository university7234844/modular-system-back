-- DropForeignKey
ALTER TABLE "Students" DROP CONSTRAINT "Students_groupId_fkey";

-- AlterTable
ALTER TABLE "Students" ALTER COLUMN "groupId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "Students" ADD CONSTRAINT "Students_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Groups"("id") ON DELETE SET NULL ON UPDATE CASCADE;
