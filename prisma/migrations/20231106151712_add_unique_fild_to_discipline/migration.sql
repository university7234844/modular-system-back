/*
  Warnings:

  - A unique constraint covering the columns `[title]` on the table `Disciplines` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Disciplines_title_key" ON "Disciplines"("title");
