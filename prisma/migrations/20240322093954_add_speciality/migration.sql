-- CreateTable
CREATE TABLE "Specialties" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "shortTitle" TEXT NOT NULL,

    CONSTRAINT "Specialties_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Specialties_id_key" ON "Specialties"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Specialties_title_key" ON "Specialties"("title");

-- CreateIndex
CREATE UNIQUE INDEX "Specialties_shortTitle_key" ON "Specialties"("shortTitle");
