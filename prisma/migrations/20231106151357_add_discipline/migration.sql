-- CreateTable
CREATE TABLE "Disciplines" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "shortTitle" TEXT NOT NULL,

    CONSTRAINT "Disciplines_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Disciplines_id_key" ON "Disciplines"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Disciplines_shortTitle_key" ON "Disciplines"("shortTitle");
