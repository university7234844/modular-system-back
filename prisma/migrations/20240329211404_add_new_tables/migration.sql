/*
  Warnings:

  - Added the required column `groupId` to the `Students` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Students" ADD COLUMN     "groupId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "Teachers" ADD COLUMN     "academicDegreeId" INTEGER,
ADD COLUMN     "academicRankId" INTEGER,
ADD COLUMN     "teacherPositionId" INTEGER;

-- CreateTable
CREATE TABLE "TeacherPositions" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,

    CONSTRAINT "TeacherPositions_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AcademicDegrees" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,

    CONSTRAINT "AcademicDegrees_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AcademicRanks" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,

    CONSTRAINT "AcademicRanks_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Groups" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "recruitmentYear" INTEGER NOT NULL,
    "educationalForm" TEXT NOT NULL,
    "specialtyId" INTEGER NOT NULL,

    CONSTRAINT "Groups_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PlanDisciplines" (
    "id" SERIAL NOT NULL,
    "lectureHoursNumber" INTEGER NOT NULL,
    "practicalHoursNumber" INTEGER NOT NULL,
    "laboratoryHoursNumber" INTEGER NOT NULL,
    "kusrHoursNumber" INTEGER NOT NULL,
    "controlWorkNumber" INTEGER NOT NULL,
    "controlForm" TEXT NOT NULL,
    "disciplineId" INTEGER NOT NULL,
    "groupId" INTEGER NOT NULL,

    CONSTRAINT "PlanDisciplines_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TeacherDisciplines" (
    "id" SERIAL NOT NULL,
    "teacherId" INTEGER NOT NULL,
    "planDisciplineId" INTEGER NOT NULL,

    CONSTRAINT "TeacherDisciplines_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "TeacherPositions_id_key" ON "TeacherPositions"("id");

-- CreateIndex
CREATE UNIQUE INDEX "TeacherPositions_title_key" ON "TeacherPositions"("title");

-- CreateIndex
CREATE UNIQUE INDEX "AcademicDegrees_id_key" ON "AcademicDegrees"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AcademicDegrees_title_key" ON "AcademicDegrees"("title");

-- CreateIndex
CREATE UNIQUE INDEX "AcademicRanks_id_key" ON "AcademicRanks"("id");

-- CreateIndex
CREATE UNIQUE INDEX "AcademicRanks_title_key" ON "AcademicRanks"("title");

-- CreateIndex
CREATE UNIQUE INDEX "Groups_id_key" ON "Groups"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Groups_title_key" ON "Groups"("title");

-- CreateIndex
CREATE UNIQUE INDEX "PlanDisciplines_id_key" ON "PlanDisciplines"("id");

-- CreateIndex
CREATE UNIQUE INDEX "TeacherDisciplines_id_key" ON "TeacherDisciplines"("id");

-- AddForeignKey
ALTER TABLE "Teachers" ADD CONSTRAINT "Teachers_teacherPositionId_fkey" FOREIGN KEY ("teacherPositionId") REFERENCES "TeacherPositions"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Teachers" ADD CONSTRAINT "Teachers_academicDegreeId_fkey" FOREIGN KEY ("academicDegreeId") REFERENCES "AcademicDegrees"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Teachers" ADD CONSTRAINT "Teachers_academicRankId_fkey" FOREIGN KEY ("academicRankId") REFERENCES "AcademicRanks"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Students" ADD CONSTRAINT "Students_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Groups"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Groups" ADD CONSTRAINT "Groups_specialtyId_fkey" FOREIGN KEY ("specialtyId") REFERENCES "Specialties"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PlanDisciplines" ADD CONSTRAINT "PlanDisciplines_disciplineId_fkey" FOREIGN KEY ("disciplineId") REFERENCES "Disciplines"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PlanDisciplines" ADD CONSTRAINT "PlanDisciplines_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Groups"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeacherDisciplines" ADD CONSTRAINT "TeacherDisciplines_teacherId_fkey" FOREIGN KEY ("teacherId") REFERENCES "Teachers"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TeacherDisciplines" ADD CONSTRAINT "TeacherDisciplines_planDisciplineId_fkey" FOREIGN KEY ("planDisciplineId") REFERENCES "PlanDisciplines"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
