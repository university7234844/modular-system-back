/*
  Warnings:

  - You are about to drop the column `fullname` on the `Users` table. All the data in the column will be lost.
  - Added the required column `birthday` to the `Users` table without a default value. This is not possible if the table is not empty.
  - Added the required column `fullName` to the `Users` table without a default value. This is not possible if the table is not empty.
  - Added the required column `sex` to the `Users` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "Roles_name_key";

-- AlterTable
ALTER TABLE "Users" DROP COLUMN "fullname",
ADD COLUMN     "birthday" TEXT NOT NULL,
ADD COLUMN     "fullName" TEXT NOT NULL,
ADD COLUMN     "sex" TEXT NOT NULL;
