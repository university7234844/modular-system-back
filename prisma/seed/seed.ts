import { PrismaClient } from '@prisma/client';
import {
  roles,
  users,
  usersRoles,
  disciplines,
  specialties,
  teacherPositions,
  academicDegrees,
  academicRanks,
  groups,
  students,
  teachers,
  planDisciplines,
  teacherDisciplines,
} from './data';

const seedData = [
  roles,
  users,
  usersRoles,
  disciplines,
  specialties,
  teacherPositions,
  academicDegrees,
  academicRanks,
  groups,
  students,
  teachers,
  planDisciplines,
  teacherDisciplines,
];

const prisma = new PrismaClient();

async function main() {
  await prisma.$connect();

  for await (const seed of seedData) {
    await prisma[seed.entity].createMany({
      data: seed.data,
      skipDuplicates: true,
    });

    console.log(`Create Seeds for Table --> ${capitalize(seed.entity)}`);
  }
}

main()
  .catch(async (error) => {
    console.log('SEED ERROR: ', error);
    await prisma.$disconnect();
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
