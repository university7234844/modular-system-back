export const teacherPositions = {
  entity: 'teacherPositions',
  data: [
    {
      // id: 1
      title: 'Преподаватель',
    },
    {
      // id: 2
      title: 'Старший преподаватель',
    },
    {
      // id: 3
      title: 'Доцент',
    },
    {
      // id: 4
      title: 'Профессор',
    },
  ],
};
