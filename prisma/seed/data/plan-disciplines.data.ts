export const planDisciplines = {
  entity: 'planDisciplines',
  data: [
    {
      // id: 1
      lectureHoursNumber: 50, //часы
      practicalHoursNumber: 20,
      laboratoryHoursNumber: 20,
      kusrHoursNumber: 8,
      controlWorkNumber: 2, //штуки
      controlForm: 'Exam',
      disciplineId: 1,
      groupId: 1,
    },
    {
      // id: 2
      lectureHoursNumber: 30,
      practicalHoursNumber: 16,
      laboratoryHoursNumber: 16,
      kusrHoursNumber: 4,
      controlWorkNumber: 2,
      controlForm: 'Credit test',
      disciplineId: 1,
      groupId: 1,
    },
    {
      // id: 3
      lectureHoursNumber: 60,
      practicalHoursNumber: 30,
      laboratoryHoursNumber: 20,
      kusrHoursNumber: 8,
      controlWorkNumber: 2,
      controlForm: 'Differentiated credit test',
      disciplineId: 2,
      groupId: 1,
    },
    {
      // id: 4
      lectureHoursNumber: 50,
      practicalHoursNumber: 26,
      laboratoryHoursNumber: 26,
      kusrHoursNumber: 6,
      controlWorkNumber: 2,
      controlForm: 'Exam',
      disciplineId: 3,
      groupId: 1,
    },
  ],
};
