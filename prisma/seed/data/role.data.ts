export const roles = {
  entity: 'roles',
  data: [
    {
      name: 'Admin',
    },
    {
      name: 'Teacher',
    },
    {
      name: 'Student',
    },
  ],
};
