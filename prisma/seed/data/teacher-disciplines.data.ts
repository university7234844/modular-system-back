export const teacherDisciplines = {
  entity: 'teacherDisciplines',
  data: [
    {
      // id: 1
      isLector: true,
      teacherId: 1,
      planDisciplineId: 1,
    },
    {
      // id: 2
      isLector: true,
      teacherId: 1,
      planDisciplineId: 2,
    },
    {
      // id: 3
      isLector: true,
      teacherId: 1,
      planDisciplineId: 3,
    },
    {
      // id: 4
      isLector: true,
      teacherId: 1,
      planDisciplineId: 4,
    },
  ],
};
