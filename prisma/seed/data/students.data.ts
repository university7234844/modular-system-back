export const students = {
  entity: 'students',
  data: [
    {
      // id: 1
      userId: 1,
      groupId: 1,
    },
    {
      // id: 2
      userId: 2,
      groupId: 1,
    },
    {
      // id: 3
      userId: 3,
      groupId: 3,
    },
  ],
};
