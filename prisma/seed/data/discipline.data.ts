export const disciplines = {
  entity: 'disciplines',
  data: [
    {
      // id: 1
      title: 'Компьютерные Системы и Сети',
      shortTitle: 'КСИС',
    },
    {
      // id: 2
      title: 'Объектно Ориентированное Программирование',
      shortTitle: 'ООП',
    },
    {
      // id: 3
      title: 'Математика',
      shortTitle: 'Мат',
    },
    {
      // id: 4
      title: 'Исследование операций',
      shortTitle: 'ИО',
    },
  ],
};
