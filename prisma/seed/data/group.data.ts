import { EducationalFormEnum } from '../../../src/common/constants';

export const groups = {
  entity: 'groups',
  data: [
    {
      // id: 1
      title: '44',
      recruitmentYear: 2020,
      educationalForm: EducationalFormEnum.FULL_TIME,
      specialtyId: 1,
    },
    {
      // id: 2
      title: '43',
      recruitmentYear: 2020,
      educationalForm: EducationalFormEnum.FULL_TIME,
      specialtyId: 3,
    },
    {
      // id: 3
      title: '41',
      recruitmentYear: 2020,
      educationalForm: EducationalFormEnum.FULL_TIME,
      specialtyId: 5,
    },
  ],
};
