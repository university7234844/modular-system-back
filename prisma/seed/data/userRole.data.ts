//Admin - id: 1
//Teacher - id: 2
//Student - id: 3

export const usersRoles = {
  entity: 'usersRoles',
  data: [
    {
      // for MikhailHerasimovich (student)
      userId: 1,
      roleId: 3,
    },
    {
      //for HannaShchamialiova (student)
      userId: 2,
      roleId: 3,
    },
    {
      // for IlyaAzarenko (student)
      userId: 3,
      roleId: 3,
    },
    {
      // for SergeyErmochenko (admin)
      userId: 4,
      roleId: 1,
    },
    {
      // for SergeyErmochenko (teacher)
      userId: 4,
      roleId: 2,
    },
  ],
};
