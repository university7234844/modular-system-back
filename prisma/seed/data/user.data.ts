export const users = {
  entity: 'users',
  data: [
    {
      login: 'MikhailHerasimovich',
      fullName: 'Михаил Герасимович',
      passwordHash:
        '$2b$10$KaBLJtsU0uLlEDHgf3ZmcujRPbPQhEyBGXnq.WFh/is9.A6drtpdq', //password: qqqqq
      sex: 'Male',
      birthday: '1002-10-08',
    },
    {
      login: 'HannaShchamialiova',
      fullName: 'Анна Щемелева',
      passwordHash:
        '$2b$10$KaBLJtsU0uLlEDHgf3ZmcujRPbPQhEyBGXnq.WFh/is9.A6drtpdq', //password: qqqqq
      sex: 'Female',
      birthday: '2002-08-25',
    },
    {
      login: 'IlyaAzarenko',
      fullName: 'Илья Азаренко',
      passwordHash:
        '$2b$10$KaBLJtsU0uLlEDHgf3ZmcujRPbPQhEyBGXnq.WFh/is9.A6drtpdq', //password: qqqqq
      sex: 'Male',
      birthday: '2002-10-10',
    },
    {
      login: 'SergeyErmochenko',
      fullName: 'Сергей Ермоченко',
      passwordHash:
        '$2b$10$KaBLJtsU0uLlEDHgf3ZmcujRPbPQhEyBGXnq.WFh/is9.A6drtpdq', //password: qqqqq
      sex: 'Male',
      birthday: '1980-12-12',
    },
  ],
};
