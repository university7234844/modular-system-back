export const academicDegrees = {
  entity: 'academicDegrees',
  data: [
    {
      // id: 1
      title: 'Кандидат физико-математических наук',
    },
    {
      // id: 2
      title: 'Доцент физико-математических наук',
    },
  ],
};
